package com.exampleswagger.demo.model;

import lombok.Data;

@Data
public class ToDoModel {
    private Integer id;
    private String task;
    private String date;
}


