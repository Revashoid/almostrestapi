package com.exampleswagger.demo.controller;

import com.exampleswagger.demo.model.ToDoModel;
import com.exampleswagger.demo.service.ToDoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ToDoController {
    private final ToDoService toDoService;

    @GetMapping("/doModel")
    public ToDoModel doModel(){
        return toDoService.doModel();
    }

    @PostMapping("/addTask")
    public void addTask(ToDoModel model){
        toDoService.addTask(model);
    }

    @GetMapping("/getList")
    public List<ToDoModel> getListModel(){
        return toDoService.getListModel();
    }

    @DeleteMapping("/deleteTask")
    public void deleteTask(Integer id){
        toDoService.deleteTask(id);
    }

    @PatchMapping("/editTask")
    public void editTask(Integer id, String newName){
        toDoService.editTask(id, newName);
    }

    @PatchMapping("/editModelTask")
    public void editModelTask(Integer id, ToDoModel model){
        toDoService.editModelTask(id, model);
    }
}
