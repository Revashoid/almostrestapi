package com.exampleswagger.demo.repository;
import com.exampleswagger.demo.model.ToDoModel;
import com.exampleswagger.demo.service.ToDoService;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class ToDoRepository{
    private final List<ToDoModel> storage = new ArrayList<>();

    public void add(ToDoModel model){
        storage.add(model);
    }

    public List<ToDoModel> getList(){
        return storage;
    }

    public void deleteTask(Integer id){
       storage.removeIf(s -> id.equals(s.getId()));
    }

    public void editTask(Integer id, String newName){
        var obj = storage.stream().filter(model -> id.equals(model.getId())).findFirst().orElse(null);
        Objects.requireNonNull(obj).setTask(newName);
    }

    public void editModelTask(Integer ObjId, ToDoModel model){
        storage.set(ObjId, model);
    }
}
