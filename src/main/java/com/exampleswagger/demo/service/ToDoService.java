package com.exampleswagger.demo.service;

import com.exampleswagger.demo.model.ToDoModel;
import com.exampleswagger.demo.repository.ToDoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ToDoService {

    private final ToDoRepository toDoRepository;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    public ToDoModel doModel(){
        var toDoModel = new ToDoModel();
        toDoModel.setId(1);
        toDoModel.setTask("Test task");
        toDoModel.setDate(dateFormat.format(new Date()));
        return toDoModel;
    }

    public void addTask(ToDoModel model){
        toDoRepository.add(model);
    }

    public List<ToDoModel> getListModel(){
        return toDoRepository.getList();
    }

    public void deleteTask(Integer id){
        toDoRepository.deleteTask(id);
    }

    public void editTask(Integer id, String newName){
        toDoRepository.editTask(id, newName);
    }

    public void editModelTask(Integer ObjId, ToDoModel model){
        toDoRepository.editModelTask(ObjId, model);
    }
}
